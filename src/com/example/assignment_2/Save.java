package com.example.assignment_2;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

//This class handles the Saving to file activity for both inverted and
//normal wav array. User inputs path in textfielda

public class Save extends Activity {
	int bufferSize;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Intent intent = getIntent();
		bufferSize = intent.getIntExtra("bufferSize", 0);
		Log.d("kulern", Integer.toString(bufferSize));
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_save);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.save, menu);
		return true;
	}
	
	public void saveFile(View view) {
		EditText tempName = (EditText) findViewById(R.id.path);
		String _path_name = tempName.getText().toString();
		BufferedOutputStream os = null;
		byte[] temp_byte=new byte[2];
		int temp_read = 0;
		byte[] trackArrayByte=new byte[bufferSize*2];
		try {
			os = new BufferedOutputStream(new FileOutputStream(
										  new File(Environment.
										  getExternalStorageDirectory().
										  getAbsolutePath()+"/"+_path_name+
										  ".wav")));
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(int i=0;i<bufferSize;i++){
			temp_byte=shortToByteArray(MainActivity.recordedPCM[i]);
			trackArrayByte[temp_read]=temp_byte[0];
			trackArrayByte[temp_read+1]=temp_byte[1];
			temp_read+=2;
		}
		
		try {
			WriteWaveFileHeader(os,(long)trackArrayByte.length, 
							   (long)trackArrayByte.length+36,(long)44100,1,
							   (long)(16*44100/8));
			for(int i=0;i<trackArrayByte.length;i++){
				os.write(trackArrayByte[i]);
			}
			Log.d("kulern", "her");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		
	}
	//Writes wav header to file
	//not completly modular as some stuff is hardcoded
	//TODO: Fix it so it becomes fully modular
	private void WriteWaveFileHeader(
            BufferedOutputStream out, long totalAudioLen,
            long totalDataLen, long longSampleRate, int channels,
            long byteRate) throws IOException {
   
    byte[] header = new byte[44];
   
    header[0] = 'R';  // RIFF/WAVE header
    header[1] = 'I';
    header[2] = 'F';
    header[3] = 'F';
    header[4] = (byte) (totalDataLen & 0xff);
    header[5] = (byte) ((totalDataLen >> 8) & 0xff);
    header[6] = (byte) ((totalDataLen >> 16) & 0xff);
    header[7] = (byte) ((totalDataLen >> 24) & 0xff);
    header[8] = 'W';
    header[9] = 'A';
    header[10] = 'V';
    header[11] = 'E';
    header[12] = 'f';  // 'fmt ' chunk
    header[13] = 'm';
    header[14] = 't';
    header[15] = ' ';
    header[16] = 16;  // 4 bytes: size of 'fmt ' chunk
    header[17] = 0;
    header[18] = 0;
    header[19] = 0;
    header[20] = 1;  // format = 1
    header[21] = 0;
    header[22] = (byte) channels;
    header[23] = 0;
    header[24] = (byte) (longSampleRate & 0xff);
    header[25] = (byte) ((longSampleRate >> 8) & 0xff);
    header[26] = (byte) ((longSampleRate >> 16) & 0xff);
    header[27] = (byte) ((longSampleRate >> 24) & 0xff);
    header[28] = (byte) (byteRate & 0xff);
    header[29] = (byte) ((byteRate >> 8) & 0xff);
    header[30] = (byte) ((byteRate >> 16) & 0xff);
    header[31] = (byte) ((byteRate >> 24) & 0xff);
    header[32] = (byte) (16 / 8);  // block align
    header[33] = 0;
    header[34] = 16;  // bits per sample
    header[35] = 0;
    header[36] = 'd';
    header[37] = 'a';
    header[38] = 't';
    header[39] = 'a';
    header[40] = (byte) (totalAudioLen & 0xff);
    header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
    header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
    header[43] = (byte) ((totalAudioLen >> 24) & 0xff);

    out.write(header, 0, 44);
}
	//converts a short to a byte array
	public static byte[] shortToByteArray(short data)
    {
        return new byte[]{(byte)(data & 0xff),(byte)((data >>> 8) & 0xff)};
    }

}
