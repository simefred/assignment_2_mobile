package com.example.assignment_2;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder.AudioSource;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

public class MainActivity extends Activity {
	private boolean stopped;
	public static short[] recordedPCM;
	
	int currentLocation;
	int length;
	AudioRecord record;
	AudioTrack playBack;
	Thread recordingThread;
	Thread playbackThread;
	int sampleRateInHz = 44100;
	int maxLengthInSeconds = 15;
	int bufferSize;
	int readShorts;
	short[] trackBuffer; 
	short[] trackForward;
	short[] trackReverse;
	
	int timer_progress;
	ProgressBar mProgressBar;
	CountDownTimer mTimer;
	
	int playSelection;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        stopped = false;
        trackBuffer = new short[maxLengthInSeconds*sampleRateInHz];
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mProgressBar = (ProgressBar)findViewById(R.id.timer);
        findViewById(R.id.play).setEnabled(false);
        findViewById(R.id.play_invert).setEnabled(false);
        findViewById(R.id.save).setEnabled(false);
        findViewById(R.id.stop).setEnabled(false);
        findViewById(R.id.save_invert).setEnabled(false);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    public void record(View view){
    	findViewById(R.id.stop).setEnabled(true);
    	findViewById(R.id.record).setEnabled(false);
    	
    	bufferSize=AudioRecord.getMinBufferSize(sampleRateInHz,
    											AudioFormat.CHANNEL_IN_MONO,
    											AudioFormat.ENCODING_PCM_16BIT);
    	try	{
    	record=new AudioRecord(	AudioSource.MIC,
    							sampleRateInHz, 
    							AudioFormat.CHANNEL_IN_MONO,
    							AudioFormat.ENCODING_PCM_16BIT,  
    							bufferSize*10);
    	} catch (IllegalArgumentException e){
    		Log.d("Record", "MEKKING FEILET");
    	}
    	
    	Log.d("Record", "Starter opptakfunksjon");
    	record.startRecording();
    	timer();
        recordingThread = new Thread(new Runnable() {
        	@Override
            public void run() {
                writeToTrack();
            }
        }, "AudioRecorder Thread");
        recordingThread.start();
    }
    
 
    
    public void writeToTrack(){
    	while(!stopped && (length+bufferSize)<(maxLengthInSeconds*
    											sampleRateInHz)){
    		
    		readShorts=record.read(trackBuffer, length, bufferSize);
    		if (readShorts > 0) {
    			length += readShorts;
    		}
    		else if (readShorts == AudioRecord.ERROR_BAD_VALUE) {
    			Log.d("ERRORrecord", "BAD VALUE");
    		}
    		else if (readShorts == AudioRecord.ERROR_INVALID_OPERATION) {
    			Log.d("ERRORrecord", "INVALID INITIALIZATION");
    		}
    		
    	}
    }
    
    public void stopRecord(View view){
    	mTimer.cancel();
    	record.stop();
		record.release();
		makeTracks();
		recordingThread=null;
		stopped = true;
        findViewById(R.id.play).setEnabled(true);
        findViewById(R.id.play_invert).setEnabled(true);
        findViewById(R.id.save).setEnabled(true);
        findViewById(R.id.save_invert).setEnabled(true);
        findViewById(R.id.stop).setEnabled(false);
    }
    
    public void makeTracks(){
    	trackForward = new short[length];
    	trackReverse = new short[length];
    	for (int i=0; i< length; i++){
    		trackForward[i] = (short) (trackBuffer[i]);
    		trackReverse[i] = trackBuffer[length-1-i];
    	}
    }
    
    public void playback(View view){

    	if (view.getId() == findViewById(R.id.play).getId()) {
    		playSelection = 0;
    	} else if (view.getId() == findViewById(R.id.play_invert).getId()) {
    		playSelection = 1;
    	}
    		
      	
      	Log.d("Audiotrack2", "Buffersize: "+String.valueOf(bufferSize));
    	try {
    		playBack = new AudioTrack(AudioManager.STREAM_VOICE_CALL,
    								sampleRateInHz,
    								AudioFormat.CHANNEL_OUT_MONO,
    								AudioFormat.ENCODING_PCM_16BIT,
    								length*2,
    								AudioTrack.MODE_STATIC);
    	} catch (IllegalArgumentException e){
    		Log.d("ERRORrecord", "Illegal Argument");    		
    	}
    	
    
    	
    	playbackThread = new Thread(new Runnable() {
        	@Override
            public void run() {
                playFromTrack(playSelection);
            }
        });
    	playbackThread.start();
    	
        
    	
    }
    
    public void playFromTrack(int choice){
    
    	if(choice == 0) {
    		readShorts = playBack.write(trackForward, 0, length);
    	} else if (choice == 1) {
    		readShorts = playBack.write(trackReverse, 0, length);
    	}

    	try {
    		playBack.play();
    	} catch (IllegalStateException e){
    		Log.d("Audiotrack2", "Failed to play.");
    	}
    }
    
    //Handles the Progress Bar, will fill it up from empty to full in 
    //15 seconds. This Function will also stop the recording after X time
    public void timer() {
    	
    	mTimer = new CountDownTimer((1*15*1000),1000){
    		@Override
    		public void onTick(long millisUntilFinished){
    			timer_progress++;
    			mProgressBar.setProgress(timer_progress);
    		}
    		@Override
    		public void onFinish(){
    			timer_progress++;
    			mProgressBar.setProgress(timer_progress);
    			stopped = true;
    			stopRecord((ImageButton)findViewById(R.id.stop));
    		}
    	}.start();
    }  
    
    public void save(View view) {
    	recordedPCM = trackForward;
    	Intent intent = new Intent(this,Save.class);
    	intent.putExtra("bufferSize", length);
    	startActivity(intent);
    }
    
    public void saveInvert(View view) {
    	recordedPCM = trackReverse;
    	Intent intent = new Intent(this, Save.class);
    	intent.putExtra("bufferSize", length);
    	startActivity(intent);
    }
}

